'use strict';

var twilio = require('twilio');

var twiml = new twilio.TwimlResponse();

twiml
	.say({ voice: 'woman' }, 'Hello caller.')
	.gather({
        action:'http://www.example.com/callFinished.php',
        finishOnKey:'*'
    }, function() {
        this.say('Press 1 for customer service')
            .say('Press 2 for British customer service', { language:'en-gb' });
    });

console.log(twiml.toString());
