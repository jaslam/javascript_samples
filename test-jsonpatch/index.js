'use strict';

/**
 * Module dependencies.
 */
var jsonpatch = require('json-patch');

console.log('running index.js');

var array = [1, 2, 3];
console.log(array);

var addValue = function(array, value) {
	array = jsonpatch.apply(array, [
		{
		    "op": "add",
		    "path": "/-",
		    "value": value
		}
	]);

	// remove any duplicates
	var lenBefore = array.length;
	array = array.reduce(function(a, b) {
		//console.log(a + ' <-->' + b);
	    if (a.indexOf(b) < 0 ) {
	    	 a.push(b);	
	    }
	    return a;
	}, []);

	var lenAfter = array.length;
	
	console.log(lenBefore === lenAfter);
	return array;
};

var removeValue = function(array, index) {
	array = jsonpatch.apply(array, [{
	    "op": "remove",
	    "path": "/"+index
	}]);

	return array;
};


try {
	// console.log(array);
	// array = addValue(array, 4);
	// console.log(array);
	// array = addValue(array, 4);
	// console.log(array);


	array = removeValue(array, 5);
	console.log(array);
} catch(e) {
	console.log(e);
}
