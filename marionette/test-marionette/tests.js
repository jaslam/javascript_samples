$( document ).ready(function() {
    console.log( "document ready!" );

    // Backbone Model
    var PersonModel = Backbone.Model.extend({
        idAttribute: 'name' 
    });

    var person1 = new PersonModel({ name: 'Alice', age: 22});
    var person2 = new PersonModel({ name: 'Bob', age: 22});

    // Backbone Collection
    var PersonsCollection = Backbone.Collection.extend({
        model: PersonModel
    });

    var persons = new PersonsCollection();
    persons.add(person1);
    persons.add(person2);

    _.each(persons.models, function(person){
        console.log('Name: %s, Age: %s', person.get('name'), person.get('age'));
    });


    // Marionette ItemView
    var RowView = Backbone.Marionette.ItemView.extend({
        template: "#row-template",
        tagName: "tr"
    });

    var TableView = Backbone.Marionette.CompositeView.extend({
        childView: RowView,
        childViewContainer: "tbody",
        template: "#table-template",
        modelEvents: {
            "change": function() {
                console.log('model changed');
            }
        },
        collectionEvents: {
            "change": function(model) {
                console.log('collection changed');
                var options = {
                    index: -1
                };
                this.collection.remove(model, options);
                this.collection.add(model, {at: options.index});
            }
        }
    });

    var grid = new TableView({
        collection: persons
    });
    grid.render();
    $('#main-content').append(grid.$el);


    persons.add({name: 'Eve', age: 24});
    persons.add({name: 'Eve2', age: 25});
    persons.add({name: 'Eve3', age: 26});
    persons.add({name: 'Eve4', age: 27});

    var i = 0;
    var intervalId = setInterval(function() {
        persons.add({
            name: 'Chris' + i,
            age: i++
        });

        if (i > 5) {
            clearInterval(intervalId);

            var intervalId2 = setInterval(function() {
                persons.remove(persons.at(i--));
                if (i < 0) {
                    clearInterval(intervalId2);
                    var model = persons.at(3);
                    model.set('age', '200');
                }
            }, 1000);
        }
    }, 1000);
});