var app = angular.module('angularApp', ['ngTagsInput', 'angucomplete-alt', 'ngNotificationsBar', 'ngSanitize']);

app.config(['notificationsConfigProvider', function (notificationsConfigProvider) {
    // auto hide
    notificationsConfigProvider.setAutoHide(true);

    // delay before hide
    notificationsConfigProvider.setHideDelay(5 * 60 * 1000);

    // support HTML
    notificationsConfigProvider.setAcceptHTML(true);

    // Set an animation for hiding the notification
    notificationsConfigProvider.setAutoHideAnimation('fadeOutNotifications');

    // delay between animation and removing the nofitication
    notificationsConfigProvider.setAutoHideAnimationDelay(1200);

}]);

app.controller('TagsController', ['$scope', '$http', '$timeout', 'notifications', 
  function($scope, $http, $timeout, notifications) {

    $timeout(function() {
      notifications.showSuccess({
        message: 'Talk scheduled successfully at ' + (new Date()).toLocaleTimeString() + 
          '. Please wait for notifications about when someone accepts to participate and the dial-in number to make the phone call for discussion.'
      });
    }, 1000);

    $scope.searchType = 'Title' + ' ▼';
    $scope.searchTypeChanged = function(searchType) {
      $scope.searchType = searchType + ' ▼';
    };

    $scope.tags = [
      { text: 'Finance' }
    ];
    $scope.loadTags = function(query) {
      return $http.get('http://localhost:3000/api/suggestions?tag=' + query, { cache: true })
        .then(function(response) {
          console.log(JSON.stringify(response.data));
          var tags = response.data;
          return tags.filter(function(tag) {
            return tag.text.toLowerCase().indexOf(query.toLowerCase()) != -1;
          });
      });
    };

    $scope.loadSuggestions = function(query) {
      console.log('query:' + JSON.stringify(query));
      return $http.get('http://localhost:3000/api/suggestions?title=' + query + '&tag=' + query + '&channel=' + query + '&host=' + query)
        .then(function(response) {
          console.log(JSON.stringify(response.data));
          //return response.data;
          var results = [];
          response.data.forEach(function(item) {
            item.image = item.type + '.png';
            item.space = '';
            results.push(item);
          });
          return results;
      });
    };

    $scope.suggestionsSelected = function(suggestion) {
      if (suggestion) {
        console.log('selected: ' + JSON.stringify(suggestion));
      }
    };
  }
]);

app.controller('SearchController', ['$scope', '$locale', '$http',
  function($scope, $locale, $http) {
  }
]);

app.controller('FormController', ['$scope', '$locale', '$http',
  function($scope, $locale) {
    $scope.error = null;
    $scope.credentials = {
      firstName: 'Jangul',
      lastName: 'Aslam',
      email: 'aslamj@gmail.com',
      phone: '1234567890',
      password: 'Embe1mpls;\'$',
      confirmPassword: 'Embe1mpls;\'$'
    };

    $scope.signup = function (isValid) {
      $scope.error = null;

      console.log('credentials: ' + JSON.stringify($scope.credentials));

      if (!isValid) {
        $scope.$broadcast('show-errors-check-validity', 'userForm');
        return false;
      }

      $http.post('/api/auth/signup', $scope.credentials).success(function (response) {
      }).error(function (response) {
        $scope.error = response.message;
      });
    };
  }
]);

app.directive('phoneInput', function($filter, $browser) {
  return {
    require: 'ngModel',
    link: function($scope, $element, $attrs, ngModelCtrl) {
      var listener = function() {
        var value = $element.val().replace(/[^0-9]/g, '');
        $element.val($filter('phoneNumber')(value, false));
      };

      // runs when we update the text field
      ngModelCtrl.$parsers.push(function(viewValue) {
        return viewValue.replace(/[^0-9]/g, '').slice(0,10);
      });

      // runs when the model gets updated on the scope directly and keeps our view in sync
      ngModelCtrl.$render = function() {
        $element.val($filter('phoneNumber')(ngModelCtrl.$viewValue, false));
      };

      $element.bind('change', listener);
      $element.bind('keydown', function(event) {
        var key = event.keyCode;
        // if the keys include the CTRL, SHIFT, ALT, or META keys, or the arrow keys, do nothing.
        // this lets us support copy and paste too
        if (key == 91 || (15 < key && key < 19) || (37 <= key && key <= 40)) {
          return;
        }
        $browser.defer(listener); // have to do this or changes don't get picked up properly
      });

      $element.bind('paste cut', function() {
          $browser.defer(listener);
      });
    }
  };
});

app.filter('phoneNumber', function () {
  return function (phone) {
    console.log(phone);
    if (!phone) { return ''; }

    var value = phone.toString().trim().replace(/^\+/, '');

    if (value.match(/[^0-9]/)) {
      return phone;
    }

    var country, city, number;

    switch (value.length) {
      case 1:
      case 2:
      case 3:
        city = value;
        break;

      default:
        city = value.slice(0, 3);
        number = value.slice(3);
    }

    if (number) {
        if (number.length>3) {
          number = number.slice(0, 3) + '-' + number.slice(3,7);
        }
        else {
          number = number;
        }
        return ("(" + city + ") " + number).trim();
    }
    else {
      return "(" + city;
    }
  };
});