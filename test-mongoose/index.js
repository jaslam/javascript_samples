'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


var UserSchema = new Schema({
  name: {
    type: String,
    default: '',
    trim: true,
    required: 'Name is required'
  },
  // users who follow you
  followers: [
    {
      type: Schema.ObjectId,
      ref: 'User'
    }
  ],
  // users who you follow
  following: [
    {
      type: Schema.ObjectId,
      ref: 'User'
    }
  ]
});

mongoose.model('User', UserSchema);

var User = mongoose.model('User');

mongoose.connect('mongodb://localhost/test-mongoose', function(err) {
	if (err) {
		console.log('connect err: ' + JSON.stringify(err));
		return;
	}

	// remove all users
	User.find().remove().exec();

	// add some users
	var user1 = new User({
		name: 'Jangul Aslam'
	});
  user1.save();
  console.log(JSON.stringify(user1));
  
	var user2 = new User({
		name: 'Minu Aslam'
	});
  user2.save();
	console.log(JSON.stringify(user2));
  
  var user3 = new User({
		name: 'Zoya Aslam'
	});
  user3.save();
	console.log(JSON.stringify(user3));
  
  var user4 = new User({
		name: 'Adiv Aslam'
	});
  user4.save();
  console.log(JSON.stringify(user4));

  console.log(JSON.stringify(user1));
    User.findOneAndUpdate({_id: user1.id }, { $addToSet: { following: user2.id } }, { new: true }, function(err, user) {
    if (err) { console.log(err); return; }
    console.log(JSON.stringify(user));
      
    User.findOneAndUpdate({_id: user1.id }, { $addToSet: { following: user2.id } }, { new: true }, function(err, user) {
      if (err) { console.log(err); return; }
      console.log(JSON.stringify(user));

      User.findOneAndUpdate({_id: user1.id }, { $addToSet: { followers: user3.id } }, { new: true }, function(err, user) {
        if (err) { console.log(err); return; }
        console.log(JSON.stringify(user));
          
        User.findOneAndUpdate({_id: user1.id }, { $pullAll: {following: [user2.id] } }, { new: true }, function(err, user) {
          if (err) { console.log(err); return; }
          console.log(JSON.stringify(user));
        });
      });    
    });
  });
})