$( document ).ready(function() {
    console.log( "document ready!" );

    var Person = Backbone.Model.extend({
        idAttribute: 'name' 
    });

    var person1 = new Person({ name: 'Alice', age: '22'});
    var person2 = new Person({ name: 'Bob', age: '22'});

    var Persons = Backbone.Collection.extend({
        model: Person
    });

    var persons = new Persons();
    persons.add(person1);
    persons.add(person2);


    // for (var i = 0; i < persons.length; ++i) {
    //     var person = persons.at(i);
    //     console.log('Name: %s, Age: %s', person.get('name'), person.get('age'));
    // }

    _.each(persons.models, function(person){
        console.log('Name: %s, Age: %s', person.get('name'), person.get('age'));
    });
});