$( document ).ready(function() {
    console.log( "document ready!" );

    var Places = Backbone.PageableCollection.extend({
        url: 'https://www.googleapis.com/freebase/v1/search?query=india',
        parse: function(response) {
            return response.result;
        }
    });

    var places = new Places();
    places.on('sync', function(model, response, options) {
        console.log('collecion sync complated');
    }, this);

    var columns = [
        {
            name: "mid",
            label: "MID",
            cell: "string"
        }, 
        {
            name: "id",
            label: "ID",
            cell: "string"
        }, 
        {
            name: "name",
            label: "Name",
            cell: "string" 
        }, 
        {
            name: "lang",
            label: "Language",
            cell: "string"
        },
        {
            name: "score",
            label: "Score",
            cell: "number"
        }
    ];

    // Initialize a new Grid instance
    var grid = new Backgrid.Grid({
      columns: columns,
      collection: places
    });

    places.on('sync', function() {
        console.log('fetch done');

        var place = places.at(0);
        var id = place.get('id');
        for (var i = 0; i < 100; ++i) {
            var model = place.clone();
            model.set('id', i + id);
            console.log(i);
            places.add(model);
        }
        // Render the grid and attach the root to your HTML document
        $("#test-backgrid").append(grid.render().el);


    }, this);

    // Fetch some countries from the url
    places.fetch({reset: true});

});