$( document ).ready(function() {
    console.log( "document ready!" );

    // Initialize the model
    var filterModel = new Backbone.Model();
    filterModel.set({
        'name': 'Test',
        'description': 'this is a test',
        'actions': ['Sell'],
        'orderType': 'Limit',
        'carCompany': 'audi'
    });

    // Define views
    var FilterView = Backbone.View.extend({
        events: {
            'click .order-type': function(e) {
                console.log('clicked radio button: ' + e.target.value);
                //this.model.set('name', e.target.value);
                // don't forget to trigger the 'change' event, when programmatically setting the value  
                $('.name').val(e.target.value).trigger('change');
            }
        },

        bindings: {
            '.name': 'name',
            '.description': 'description',
            '.action': 'actions',
            '.order-type': 'orderType',
            '.car-company': 'carCompany'
        },

        render: function () {
            this.stickit();
        }
    });

    var OutputView = Backbone.View.extend({
        initialize: function () {
            this.listenTo(this.model, 'change', this.render);
        },

        render: function () {
            var data = this.model.toJSON();
            $('#name-value').html(JSON.stringify(data.name));
            $('#description-value').html(JSON.stringify(data.description));
            $('#actions-value').html(JSON.stringify(data.actions));
            $('#order-type-value').html(JSON.stringify(data.orderType));
            $('#car-company-value').html(JSON.stringify(data.carCompany));
        }
    });

    // Instantiate and render views
    var filterView = new FilterView({
        model: filterModel,
        el: '#filter'
    });

    var outputView = new OutputView({
        model: filterModel,
        el: '#output'
    });

    filterView.render();
    outputView.render();

});