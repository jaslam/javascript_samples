'use strict';

function jsInheritance() {
    var Base = function(x, y) {
        
        // private variables
        var name = "Base";

        // private method
        var foo = function() {
            console.log('foo()');
        };

        // public variables
        this.x = x;
        this.y = y;

        // public method 
        this.toString = function() {
            console.log('name: ' + name + ', x: ' + this.x + ', y: ' + this.y);
        };
    };

    Base.prototype.logMessage = function(message) {
        console.log('Base: ' + message);
    };

    var base = new Base(10, 20);
    base.toString();
    base.logMessage('I\'m Base');


    var Derrived = function(x, y, z) {
        var name = 'Derrived';

        // call the base class ctor by using .call or .apply
        //Base.call(this, x, y);    // 'call' takes arguments seperated by commas
        Base.apply(this, [x, y]);   // 'apply' takes arguments in an array

        this.z = z;

        this.toString = function() {
            console.log('name: ' + name + ', x: ' + this.x + ', y: ' + this.y + ' z: ' + this.z);
        }
    };

    // sets the inheritance
    //Derrived.prototype = Object.create(Base.prototype);
    Derrived.prototype = new Base;


    Derrived.prototype.logMessage = function(message) {
        console.log('Derrived: ' + message);
    };


    var derrived = new Derrived(50, 60, 70);
    derrived.toString();
    derrived.logMessage('I\'m Base');
}

var logMessage = function(str) {
    var now = new Date();
    console.log('[%d/%d/%d %d:%d:%d.%3d] => %s', 
        now.getMonth(), now.getDate(), now.getFullYear(), 
        now.getHours(), now.getMinutes(), now.getSeconds(), now.getMilliseconds(), 
        str
    );
};

function jsBinding() {
    var Some = function() {
        var a = 10;
        this.b = 20;

        logMessage('starting timeout...');
        // setting the timeout
        setTimeout((function(c, d) {
            logMessage('after timeout');
            logMessage(a + ' , ' + this.b + ' , ' + c + ' , ' + d);
            logMessage
        }).bind(this, 30, 40), 1000);
    };

    var some = new Some();
};

function binarySearchTree() {
    var BSTNode = function(data) {
        this.data = data;
      this.left = null;
      this.right = null;
    };
    //BSTNode.prototype.print = function() {
    //  console.log(JSON.stringify(node));
    //};

    var BST = function(node) {
        this.root = node;
    };
    BST.prototype.insertRecursive = function(root, node) {
        if (!node) return;
      
        if (!root.left && node.data <= root.data) {
        root.left = node;   // insert on left
        return;
      } else if (!root.right && node.data > root.data) {
        root.right = node; //insert on right
        return;
      } 
      
      if(node.data <= root.data) {
        this.insertRecursive(root.left, node);
      } else {
        this.insertRecursive(root.right, node);
      }
    };
    BST.prototype.preOrderTraversalRecursive = function(node) {
        if (!node) return;
      
      console.log(node.data);
      this.preOrderTraversalRecursive(node.left);
      this.preOrderTraversalRecursive(node.right);
    };
    BST.prototype.inOrderTraversalRecursive = function(node) {
        if (!node) return;
      
      this.inOrderTraversalRecursive(node.left);
      console.log(node.data);
      this.inOrderTraversalRecursive(node.right);
    };
    BST.prototype.postOrderTraversalRecursive = function(node) {
        if (!node) return;
      
      this.postOrderTraversalRecursive(node.left);
      this.postOrderTraversalRecursive(node.right);
      console.log(node.data);
    };

    var bst = new BST(new BSTNode(50));
    bst.insertRecursive(bst.root, new BSTNode(40));
    bst.insertRecursive(bst.root, new BSTNode(80));
    bst.insertRecursive(bst.root, new BSTNode(30));
    bst.insertRecursive(bst.root, new BSTNode(20));
    bst.insertRecursive(bst.root, new BSTNode(35));
    bst.insertRecursive(bst.root, new BSTNode(45));
    bst.insertRecursive(bst.root, new BSTNode(75));
    bst.insertRecursive(bst.root, new BSTNode(90));
    bst.insertRecursive(bst.root, new BSTNode(85));


    console.log('pre-order-traversal-recursive');
    bst.preOrderTraversalRecursive(bst.root);
    console.log('in-order-traversal-recursive');
    bst.inOrderTraversalRecursive(bst.root);
    console.log('post-order-traversal-recursive');
    bst.postOrderTraversalRecursive(bst.root);

}

$( document ).ready(function() {
    console.log( "document ready!" );
    
    binarySearchTree();

    //jsInheritance();

    //jsBinding();

    // var message = 'Aslam Test invited you for Talk "Test2" on Thu Feb 04 2016 at 7:00 PM. Please accept this request at https://protalkhub.herokuapp.com/talks/56b3e1eba536190e00b37567.';

    // var html = '';
    // if (message.match(/ at https?:\/\/(protalkhub.herokuapp.com|localhost:3000)/i)) {
    //   var parts = message.split(/ at https?:\/\/(protalkhub.herokuapp.com|localhost:3000)/i);
    //   html = parts[0].split('request')[0];
    //   html += '<a href="' + parts[2].replace('.', '') + '"> request</a>.';
    // }
    // console.log(html);


    // message = 'Aslam Test invited you for Talk "Test2" on Thu Feb 04 2016 at 7:00 PM. Please accept this request at http://localhost:3000/talks/56b3e1eba536190e00b37567.';

    // html = '';
    // if (message.match(/ at https?:\/\/(protalkhub.herokuapp.com|localhost:3000)/i)) {
    //   var parts = message.split(/ at https?:\/\/(protalkhub.herokuapp.com|localhost:3000)/i);
    //   html = parts[0].split('request')[0];
    //   html += '<a href="' + parts[2].replace('.', '') + '"> request</a>.';
    // }
    // console.log(html);

    // // if (message.includes(' at https?:\/\/(protalkhub.herokuapp.com|localhost:3000)')) {
    // //   var parts = message.split(' at https://protalkhub.herokuapp.com');
    // //   html = parts[0].split('request')[0];
    // //   html += '<a href="' + parts[1] + '"> request</a>.';
    // // }

    // console.log(html);
});